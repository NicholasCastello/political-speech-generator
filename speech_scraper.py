import re
import requests
import subprocess
import webbrowser
import pkg_resources
from politicians import POLITICIAN_NAMES


# Constants
BASE_URL = "https://www.rev.com/blog/transcripts/page/"



def getTranscriptsOnCurrentPage(person_url):
    """
        Get a list of URLs to each transcript for the given politican
    """
    response = requests.get(person_url)
    soup = BeautifulSoup(response.text, 'html.parser')

    div_results = soup.find_all("div", {"class": "fl-post-column"})

    transcript_links = []

    for div in div_results:
        link = div.find('a')
        transcript_links.append(link.get("href"))

    return transcript_links



def getNumPages(page_url):
    """
        Get the number of pages for the search result so we know how many to iterate over
    """
    response = requests.get(page_url)
    soup = BeautifulSoup(response.text, 'html.parser')

    max_page = 0
    for page_num in soup.find_all("a", {"class": "page-numbers"}):
        curr_num = page_num.text
        if (curr_num.isdigit()):
            curr_num = int(curr_num)
            if (curr_num > max_page):
                max_page = curr_num

    print("There are {} pages of this politician".format(max_page))
    return max_page



def getSpeech(transcript_urls, politician):
    """
        Iterate through the list of transcript URLs and append all speech to a file
    """
    for transcript_url in transcript_urls:
        response = requests.get(transcript_url)
        soup = BeautifulSoup(response.text, 'html.parser')

        # This is the ENTIRE transcript
        speech_div = soup.find(class_="fl-callout-text")

        # Write each paragraph from the given speaker 
        # to a file, one paragrapgh at a time
        with open(f'{politician}.txt', "a") as file:    # TODO: add case for deleting file if it already exists
            solo_speech = []

            # Each paragraph tag in the transcript div
            for paragraph in speech_div.find_all('p'):   

                speech_para = paragraph.text
                colon_index = speech_para.find(':')
                nl_index = speech_para.find('\n')

                speaker = speech_para[:colon_index]

                for politician_alt_name in POLITICIAN_NAMES[politician]:

                    if (politician_alt_name.lower() == speaker.lower()):
                        speech = _sanitize(speech_para[nl_index:])

                        #print(f'\nSPEAKER: {speaker}\nSPEECH: {speech}')
                        solo_speech.append(speech)
                        break

            file.write("".join(solo_speech))


def drawProgressBar(current, end):
    """
        Draw a progress bar to keep the user informed about status
    """
    progress = ""
    scale = 50
    percent_complete = int((current / end) * 100)
    normalized_complete = int((current / end) * scale)

    # Normalize data to scale between 0 and 10
    for i in range(normalized_complete):
        progress += "*"

    for i in range(scale - len(progress)):
        progress += "-"

    print(f'{progress} {percent_complete}% ({current}/{end})')


# TODO: Finish this. Work in progress
def _sanitize(speech)
    # Remove ellipses ... (replace with period?) - investigate why are there so many in the data set
    # Remove brackets with timecodes Ex.  
    # Replace contractions with the two full words ?
    # Remove dollar sign $. Ex. $3 should be changed to 3 dollars
    # Its important to keep periods but should commas be kept? they may confuse the AI since for ex it will read "go" and "go," as two diff words
    # What about other punctuation like ? and ! - Replace with periods?
    # What about hyphens? 
    # get rid of descriptions Ex. (silence)

    speech = re.sub(r"i'm", "i am", speech)
    speech = re.sub(r"i’m", "i am", speech)

    speech = re.sub(r"he's", "he is", speech)
    speech = re.sub(r"he’s", "he is", speech)

    speech = re.sub(r"she's", "she is", speech)
    speech = re.sub(r"she’s", "she is", speech)

    speech = re.sub(r"it's", "it is", speech)
    speech = re.sub(r"it’s", "it is", speech)

    speech = re.sub(r"that's", "that is", speech)
    speech = re.sub(r"that’s", "that is", speech)

    speech = re.sub(r"what's", "what is", speech)
    speech = re.sub(r"what’s", "what is", speech)

    speech = re.sub(r"where's", "where is", speech)
    speech = re.sub(r"where’s", "where is", speech)

    speech = re.sub(r"there's", "there is", speech)
    speech = re.sub(r"there’s", "there is", speech)

    speech = re.sub(r"who's", "who is", speech)
    speech = re.sub(r"who’s", "who is", speech)

    speech = re.sub(r"how's", "how is", speech)
    speech = re.sub(r"how’s", "how is", speech)

    speech = re.sub(r"\'ll", " will", speech)
    speech = re.sub(r"’ll", " will", speech)

    speech = re.sub(r"\'ve", " have", speech)
    speech = re.sub(r"’ve", " have", speech)

    speech = re.sub(r"\'re", " are", speech)
    speech = re.sub(r"’re", " are", speech)

    speech = re.sub(r"\'d", " would", speech)
    speech = re.sub(r"’d", " would", speech)

    speech = re.sub(r"won't", "will not", speech)
    speech = re.sub(r"won’t", "will not", speech)

    speech = re.sub(r"can't", "cannot", speech)
    speech = re.sub(r"can’t", "cannot", speech)

    speech = re.sub(r"n't", " not", speech)
    speech = re.sub(r"n’t", " not", speech)

    speech = re.sub(r"n'", "ng", speech)
    speech = re.sub(r"n’", "ng", speech)

    speech = re.sub(r"'bout", "about", speech)
    speech = re.sub(r"’bout", "about", speech)

    speech = re.sub(r"'til", "until", speech)
    speech = re.sub(r"’til", "until", speech)

    speech = re.sub(r"c'mon", "come on", speech)
    speech = re.sub(r"c’mon", "come on", speech)

    speech = re.sub("%", " percent", speech)         # takes case of 3%
    speech = re.sub("\+", " plus", speech)           # takes care of A+
    speech = re.sub("&", "and", speech)              # takes care of Johnson & Johnson
    speech = re.sub(r"\(.{2,}\)", "", speech)       # takes care of (silence)
    speech = re.sub(r"#", "number ", speech)         # takes care of #1
    
    speech = re.sub("\n", "", speech)   # Remove new lines?

    sentence = re.sub("[-*/()\"’'#/@$;:<>{}`+=~|!?,…]", "", sentence)   # takes care of all other characters

    return speech



def main():
    for politician in POLITICIAN_NAMES.keys():
        print("Finding speeches for {}".format(politician))

        curr_page = 1
        last_page = 1
        while (curr_page <= last_page):
            person_url = f'{BASE_URL}{curr_page}?s={politician.replace(" ", "+")}'

            if (curr_page == 1):
                # find the max num pages
                last_page = getNumPages(person_url)
                print(f'Last page is now {last_page}')

            transcript_links = getTranscriptsOnCurrentPage(person_url)

            getSpeech(transcript_links, politician)

            drawProgressBar(curr_page, last_page)

            curr_page += 1



def installDependencies():
    required = {'bs4'}
    installed = {pkg.key for pkg in pkg_resources.working_set}
    missing = required - installed

    if missing:
        print("Installing dependencies...")
        python = sys.executable
        subprocess.check_call([python, '-m', 'pip', 'install', *missing], stdout=subprocess.DEVNULL)



if __name__=="__main__":

    installDependencies()

    try:
        from bs4 import BeautifulSoup
        main()
    except ImportError as e:
        print("ERROR: Could not install one or more required dependency.\n{}\nQuitting...".format(e))
    except Exception as e:
        print("ERROR ({}): {}".format(type(e), e))
        


